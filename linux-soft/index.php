<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("linux-soft");
?>

<?//Список элементов ORM?>
<?$APPLICATION->IncludeComponent(
	"custom:custom.element.list-orm",
	".default",
	array(
		"IBLOCK_ID" => "4",
		"COMPONENT_TEMPLATE" => ".default",
		"IBLOCK_TYPE" => "Linux_Soft",
		"CACHE_TYPE" => "A",
		"CACHE_TIME" => "3600",
		"SIZE_HEIGHT" => "200",
		"SIZE_WIDTH" => "200",
	),
	false
);?>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
