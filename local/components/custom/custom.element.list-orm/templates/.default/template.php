<?php
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */

$this->setFrameMode(true);
?>

<div class="content">
    <? foreach ($arResult["ITEMS"] as $arItem): ?>
        <div class="content__element">
            <a class="content__element_link-name" href="#">
                <h3><?=$arItem["NAME"];?></h3>
            </a>
            <a class="content__element_link-image" href="#">
                <img src="<?=$arItem["PREVIEW_PICTURE_SRC_SMALL"]?>" alt="<?=$arItem["NAME"];?>">
            </a>
            <h4 class="content__element_description-title">Имя создателя: <span class="content__element_description-link"><?=$arItem["USER_NAME"];?></span></h4>
            <h4 class="content__element_description-title">Путь к картинке: <span class="content__element_description-link"><?=$arItem["PREVIEW_PICTURE_SRC"];?></span></h4>
            <h4 class="content__element_description-title">Последние версии: <span class="content__element_description-link"><?=$arItem["LAST_VERSION_LIST"];?></span></h4>
            <div class="content__element_description-logo">
                <h4 class="content__element_description-title">Логотип:</h4>
                <img src="<?=$arItem['LOGOTIP_SRC']?>" alt="">
            </div>
            <h4 class="content__element_description-title">Анонс:</h4>
            <p class="content__element_description"><?=$arItem["PREVIEW_TEXT"];?></p>
        </div>
    <? endforeach; ?>
</div>
