<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$MESS["EXAMPLE_COMPSIMPLE_PROP_SETTINGS"] = "Выбор инфоблока";
$MESS["EXAMPLE_COMPSIMPLE_PROP_SIZE"] = "Параметры изображения (в пикселях)";
$MESS["EXAMPLE_COMPSIMPLE_PROP_IBLOCK_TYPE"] = "Тип инфоблока";
$MESS["EXAMPLE_COMPSIMPLE_PROP_IBLOCK_ID"] = "Инфоблок";
$MESS["EXAMPLE_COMPSIMPLE_PROP_SECTION_ID"] = "Раздел инфоблока";
$MESS["EXAMPLE_COMPSIMPLE_PROP_SECTION_WIDTH"] = "Ширина изображения";
$MESS["EXAMPLE_COMPSIMPLE_PROP_SECTION_HEIGHT"] = "Высота изображения";