<?php
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
    die();
}

use \Bitrix\Main\Application,
    \Bitrix\Main\Loader;

class ExampleCompSimple extends CBitrixComponent
{
    public  $arItem = [];
    private $_request;
    
    /**
     * Проверка наличия модулей, требуемых для работы компонента
     * @return bool
     * @throws Exception
     */
    private function _checkModules()
    {
        if (!Loader::includeModule('iblock')) {
            throw new \Exception('Не загружены модули необходимые для работы модуля');
        }
        return true;
    }
    
    /**
     * Подготовка параметров компонента
     *
     * @param $arParams
     *
     * @return mixed
     */
    public function onPrepareComponentParams($arParams)
    {
        // тут пишем логику обработки параметров, дополнение параметрами по умолчанию
        // и прочие нужные вещи
        
        return $arParams;
    }
    
    /**
     * Получение элементов инфоблока
     *
     * @param $arParams
     *
     * @return string
     */
    public function getElements()
    {
        
        \Bitrix\Main\Loader::includeModule("iblock");
        \Bitrix\Main\Loader::includeModule('get.property');
        
        // $query = LandingPropsTable::query()
        //     ->setSelect(
        //         [
        //             'NAME',
        //             'PREVIEW_PICTURE',
        //             'PREVIEW_TEXT',
        //             'PROP.LOGOTIP',
        //             'PROP.AVAILABLE',
        //             'LAST_VERSION_LIST',
        //             'USER_NAME'
        //         ]
        //     )
        //     ->setGroup(['ID'])
        //     ->where('IBLOCK_ID', $this->arParams['IBLOCK_ID']);
        //
        // $result = $query->exec();
        // print_r($query->getQuery());
        //
        // echo '<pre>';
        // print_r($result->fetchAll());
        // echo '</pre>';
        // while ($arItems = $result->fetch()) {
        //     $arItems["PREVIEW_PICTURE_SRC"] = CFile::GetPath($arItems["PREVIEW_PICTURE"]);
        //     $arItems["LOGOTIP_SRC"]         = CFile::GetPath($arItems["GET_PROPERTY_ORM_LANDING_PROPS_PROP_LOGOTIP"]);
        //     $arResult[] = $arItems;
        // }
        
        $result = Get\Property\Orm\LandingPropsTable::getList([
            'select' => [
                'NAME',
                'PREVIEW_PICTURE',
                'PREVIEW_TEXT',
                'PROP.LOGOTIP',
                'PROP.AVAILABLE',
                'LAST_VERSION_LIST',
                'USER_NAME',
            ],
            'group'  => [
                'ID',
            ],
            'filter' => [
                'IBLOCK_ID' => $this->arParams['IBLOCK_ID'],
            ],
        ]);
        
        while ($arItems = $result->fetch()) {
            $arItems["PREVIEW_PICTURE_SRC"] = CFile::GetPath($arItems["PREVIEW_PICTURE"]);
            $arItems["LOGOTIP_SRC"]         = CFile::GetPath($arItems["GET_PROPERTY_ORM_LANDING_PROPS_PROP_LOGOTIP"]);
            $arResult["ITEMS"][]            = $arItems;
        }
        
        $this->arResult = $arResult;
    }
    
    /**
     * Ресайз изображений
     *
     * @param $arParams
     *
     * @return string
     */
    public function resizeImage()
    {
        foreach ($this->arResult["ITEMS"] as $elements) {
            $fileSrc                               = CFile::ResizeImageGet(
                $elements["PREVIEW_PICTURE"],
                [
                    "width"  => $this->arParams["SIZE_WIDTH"],
                    "height" => $this->arParams["SIZE_HEIGHT"],
                    BX_RESIZE_IMAGE_PROPORTIONAL,
                    true,
                ]);
            $elements["PREVIEW_PICTURE_SRC_SMALL"] = $fileSrc['src'];
            $arResult["ITEMS"][]                   = $elements;
        }
        
        $this->arResult = $arResult;
    }
    
    /**
     * Запуск компонента
     * @return mixed|void
     * @throws \Bitrix\Main\SystemException
     */
    public function executeComponent()
    {
        $this->_checkModules();
        
        $this->_request = Application::getInstance()->getContext()->getRequest();
        
        $this->getElements();
        $this->resizeImage();
        
        $this->includeComponentTemplate();
    }
}
