<?php
namespace Get\Property\Orm;

use Bitrix\Main,
    Bitrix\Main\Localization\Loc;
Loc::loadMessages(__FILE__);

/**
 * Class ElementPropS4Table
 *
 * Fields:
 * <ul>
 * <li> IBLOCK_ELEMENT_ID int mandatory
 * <li> PROPERTY_43 string optional
 * <li> PROPERTY_44 int optional
 * <li> PROPERTY_45 string optional
 * </ul>
 *
 * @package Bitrix\Iblock
 **/

class ElementPropS4Table extends Main\Entity\DataManager
{
    /**
     * Returns DB table name for entity.
     *
     * @return string
     */
    public static function getTableName()
    {
        return 'b_iblock_element_prop_s4';
    }
    
    /**
     * Returns entity map definition.
     *
     * @return array
     */
    public static function getMap()
    {
        return array(
            'IBLOCK_ELEMENT_ID' => array(
                'data_type' => 'integer',
                'primary' => true,
                'title' => Loc::getMessage('ELEMENT_PROP_S4_ENTITY_IBLOCK_ELEMENT_ID_FIELD'),
            ),
            'AVAILABLE' => array(
                'data_type' => 'integer',
                'column_name' => 'PROPERTY_43',
            ),
        
            'LOGOTIP' => array(
                'data_type' => 'integer',
                'column_name' => 'PROPERTY_44',
            )
        );
    }
}
