<?php

namespace Get\Property\Orm;

use Bitrix\Iblock\PropertyEnumerationTable;
use Bitrix\Main\Entity;
use Bitrix\Iblock\ElementTable;
use Bitrix\Main\Loader;
use Bitrix\Main\Localization\Loc;
use Bitrix\Main\ORM\Query\Join;
use Bitrix\Main\UserTable;


Loc::loadMessages(__FILE__);

Loader::includeModule('iblock');

/**
 * Class LandingPropsTable
 * @package Get\Property\Orm
 */
class LandingPropsTable extends ElementTable
{
    public static function getMap()
    {
        $arFields = parent::getMap();
        
        $arFields['PROP'] = new Entity\ReferenceField(
            'PROP',
            ElementPropS4Table::class,
            Join::on('this.ID', 'ref.IBLOCK_ELEMENT_ID')
        );
        
        $arFields['USER'] = new Entity\ReferenceField(
            'USER',
            UserTable::class,
            Join::on('this.CREATED_BY', 'ref.ID'),
            ['join_type' => 'LEFT']
        );
    
        $arFields['USER_NAME'] = new Entity\ExpressionField(
            'USER_NAME',
            'GROUP_CONCAT(DISTINCT %s)',
            ['USER.NAME']
        );
        
        $arFields['PROPS'] = new Entity\ReferenceField(
            'PROPS',
            ElementPropM4Table::class,
            Join::on('this.ID', 'ref.IBLOCK_ELEMENT_ID')
        );
        
        $arFields['LAST_VERSION'] = new Entity\ReferenceField(
            'LAST_VERSION',
            PropertyEnumerationTable::class,
            Join::on('this.PROPS.VALUE', 'ref.ID')
        );
        
        $arFields['LAST_VERSION_LIST'] = new Entity\ExpressionField(
            'LAST_VERSION_LIST',
            'GROUP_CONCAT(DISTINCT %s SEPARATOR ", ")',
            ['LAST_VERSION.VALUE']
        );
        
        return $arFields;
    }
}