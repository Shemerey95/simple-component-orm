<?php IncludeModuleLangFile(__FILE__);
if (class_exists("get_property"))
    return;

use Bitrix\Main\ModuleManager;
use Bitrix\Main\Localization\Loc as Loc;

class get_property extends CModule
{
    
    public $MODULE_ID = "get.props";
    
    public function __construct()
    {
        if (file_exists(__DIR__ . "/version.php")) {
            include_once(__DIR__ . "/version.php");
            $this->MODULE_ID           = str_replace("_", ".", get_class($this));
            $this->MODULE_VERSION      = $arModuleVersion["VERSION"];
            $this->MODULE_VERSION_DATE = $arModuleVersion["VERSION_DATE"];
            $this->MODULE_NAME         = 'Orm-property';
            $this->MODULE_DESCRIPTION  = 'Описание сущности пользовательских свойств инфоблока';
            $this->PARTNER_NAME        = 'Property';
            $this->PARTNER_URI         = 'Partner URL';
        }
    }
    
    public function DoInstall()
    {
        ModuleManager::registerModule($this->MODULE_ID);
        \Bitrix\Main\Loader::includeModule($this->MODULE_ID);
    }
    
    public function DoUninstall()
    {
        ModuleManager::unRegisterModule($this->MODULE_ID);
        \Bitrix\Main\Loader::includeModule($this->MODULE_ID);
    }
}
